import React from 'react'
import Main from '../template/content/Content'
import Card from '../card/Card'
export default props =>
<Main {...props}>
    <div className="home">
        <Card/>
    </div>
</Main>