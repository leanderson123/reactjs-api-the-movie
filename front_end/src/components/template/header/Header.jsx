import './Header.css'
import React from 'react'

export default props =>
<header className="header">
    <div>
        <h1><i className={`fa fa-${props.icon}`}/>{props.pag}</h1>
        <p>List Movies</p>
    </div>
</header>