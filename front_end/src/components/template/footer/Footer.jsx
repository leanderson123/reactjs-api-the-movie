import './Footer.css'
import React from 'react'

export default () =>
<footer className="footer">
    <p>&copy; Feito por <strong>Leanderson de Oliveira Santana</strong></p>
</footer>