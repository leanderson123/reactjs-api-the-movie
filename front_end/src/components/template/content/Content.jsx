import './Content.css'
import React from 'react'
import Header from '../header/Header'

export default props =>
<React.Fragment>
    <Header {...props}/>
    <main className="main"> 
        {props.children}
    </main>
</React.Fragment>