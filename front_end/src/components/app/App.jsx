import './App.css'
import React,{Component} from 'react'
import 'font-awesome/css/font-awesome.min.css'
import Nav from '../template/nav/Nav'

import Home from '../home/Home'
import Footer from '../template/footer/Footer'

export default class App extends Component {
    render(){
        return(
            <div className="app">
                <Nav />
                <Home icon="home" pag="Home"/>
                <Footer/>
            </div>
        )
    }
}  