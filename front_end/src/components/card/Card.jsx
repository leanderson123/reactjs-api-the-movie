import './Card.css'
import React from 'react'
import CardImg from './card-img/CardImg'
import CardTitle from './card-title/CardTitle'
import CardBody from './card-body/CardBody'

export default props =>
<div className="card">
    <CardImg/>
    <CardTitle/>
    <CardBody/>
</div>