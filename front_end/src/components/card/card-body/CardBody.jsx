import './CardBody.css'
import React from 'react'

export default props =>
<div className="cardBody">
    <p>
    Um homem não-assertivo e derrotado descobre que tem um alter-ego, um anarquista / terrorista sociopata que está planejando a destruição da sociedade, e deve silenciar o elemento destrutivo de sua personalidade antes que esquemas calamitosos de bombardeio ocorram em todo o país.
    </p>
</div>