import './CardImg.css'
import React from 'react'

export default props => 
<img 
src="https://www.foxmovies.com/s3/dev-temp/en-US/__5979479c91993-063cba48f96602b1b3e9408fc56367f3e7bc08d5-b0b8b5e7e78fb391.jpg"
alt="poster movie"/>